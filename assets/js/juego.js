const miModulo = (() => {
    'use strict'

    let deck = [],
        puntosJugadores = [];

    const tipos = ['C', 'D', 'H', 'S'],
          especiales = ['A', 'J', 'K', 'Q'];

    // Referencias del html
    const btnPedir = document.querySelector('#btnPedir'),
          btnDetener = document.querySelector('#btnDetener'),
          btnNuevo = document.querySelector('#btnNuevo'),
          divCartasJugadores = document.querySelectorAll('.divCartas'),
          puntosHTML = document.querySelectorAll('small');

    /**
     * Inicializa el juego y se define cantidad de jugadores
     * @param {*} cantidadJugadores 
     */
    const inicializarJuego = (cantidadJugadores = 2) => {
        deck = crearDeck();
        puntosJugadores = [];
        for (let i = 0; i < cantidadJugadores; i++) {
            puntosJugadores.push(0);
        }
        puntosHTML.forEach( elem => elem.innerText = 0 );
        divCartasJugadores.forEach( elem => elem.innerHTML = '' );

    }

    /**
     * @returns maso de cartas mezcladas
     */
    const crearDeck = () => {
        deck = [];
        for (let i = 2; i <= 10; i++)
            for (const tipo of tipos)
                deck.push(i + tipo);

        for (const tipo of tipos)
            for (const esp of especiales)
                deck.push(esp + tipo);

        return _.shuffle(deck); 
    }

    /**
     * @returns una carta del mazo
     */
    const pedirCarta = () => {
        if (deck.length === 0) return console.warn('No hay mas cartas');
        return deck.pop();
    }

    /**
     * @param carta carta del maso 
     * @returns el valor de la carta ingresada
     */
    const valorCarta = (carta) => {
        const valor = carta.substring(0, carta.length - 1);
        return !isNaN(valor) ? valor * 1 : (valor === 'A') ? 11 : 10;
    }

    /**
     * @param carta carta del maso
     */
    const crearCarta = (carta, turno) => {
        const imgCarta = document.createElement('img');
        imgCarta.src = `assets/cartas/${carta}.png`;
        imgCarta.classList.add('carta');
        divCartasJugadores[turno].append(imgCarta);
    }

    const borrarCartas = () => {
        cartasJugador.innerHTML = '';
        cartasComputadora.innerHTML = '';
    }

    const deshabilitarPedirDetener = () => {
        btnPedir.disabled = true;
        btnDetener.disabled = true;
    }

    const habilitarPedirDetener = () => {
        btnPedir.disabled = false;
        btnDetener.disabled = false;
    }

    /**
     * El ultimo valor del arreglo de puntosJugadores siempre es la computadora
     * @param {*} turno 
     */
    const acumularPuntos = ( carta, turno ) => {
        puntosJugadores[turno] += valorCarta(carta);
        puntosHTML[turno].innerText = puntosJugadores[turno];
        return puntosJugadores[turno];
    }

    // turno de la computadora 
    const turnoComputadora = (puntosMinimos) => {
        let puntosComputadora = 0;
        do {
            const carta = pedirCarta();
            puntosComputadora =  acumularPuntos( carta, puntosJugadores.length - 1 );
            crearCarta(carta, puntosJugadores.length - 1 );
        } while ((puntosComputadora < puntosMinimos) && (puntosMinimos <= 21));
        determinarGanador();
    }

    const determinarGanador = () => {
        const [ puntosMinimos, puntosComputadora ] = puntosJugadores;
        setTimeout(() => {
            if (puntosMinimos === puntosComputadora) {
                alert('Empate');
            } else if (puntosMinimos > 21 || (puntosComputadora <= 21) && (puntosComputadora > puntosMinimos)) {
                alert('gano el bot');
            } else {
                alert('ganaste');
            }
        }, 100);
    }

    // Eventos
    btnPedir.addEventListener('click', () => {
        const carta = pedirCarta();
        const puntosJugador =  acumularPuntos( carta, 0 );
        crearCarta(carta, 0);
        if (puntosJugador > 21) {
            deshabilitarPedirDetener();
            turnoComputadora(puntosJugador);
        } else if (puntosJugador === 21) {
            deshabilitarPedirDetener();
            turnoComputadora(puntosJugador);
        }
    });

    btnDetener.addEventListener('click', () => {
        deshabilitarPedirDetener();
        turnoComputadora(puntosJugadores[0]);
    })

    btnNuevo.addEventListener('click', () => {
        habilitarPedirDetener();
        inicializarJuego();
    })
    
    return {
        nuevoJuego: inicializarJuego
    }

})();